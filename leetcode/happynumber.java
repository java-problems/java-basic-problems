class Solution {
    public boolean isHappy(int n) {

        boolean result = false;

        for (int i = 0; i < n; i++) {
            int digit = 0;
            int sum = 0;
            while (n > 0) {
                digit = n % 10;

                sum = sum + (digit * digit);

                n = n / 10;
            }
            n = sum;
            // System.out.println(n);

            if (sum == 1) {
                result = true;
            }
        }

        if (result == true) {
            System.out.println("happy");
            return true;
        }
         else {
            System.out.println("not happy");
            return false;
        }
    }
}
