package problem;

public class Power {
public static void main(String[] args) {

	
	Power pp = new Power();
	//pp.power(5,2);     //5^2  
	
	int base=1;        //1^1   2^2   3^3  4^4   5^5
	while(base<=5) {
		
		pp.power(base,base); 
		base++;
	}	
}

public void power(int base1,int power1) {
	
	int base=base1;
	int power=power1;
	int result=1;
	
	while(power>0) {
		
		result=result*base;
		
		power--;
	}
	System.out.println(result);
}
}
