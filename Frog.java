
/* one frog is in the 30 feet wall its tries to climb from the 
 * wall if the frog climb 2feet which descent down direction 1 feet in one day 
 * inwhich days the frog will comeback to land
 */

package problem;


public class Frog {
	public static void main(String[] args) {
		
		int  ft=30;
		int down=1;
		int up=2;
		int day=0;
		
		while(ft>0) {
			
			ft=ft-up+down;
			day=day+1;
		}
		System.out.println( "the frog climb in " +day+" days");
	}

}
